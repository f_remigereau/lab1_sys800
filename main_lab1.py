"""
SYS800 - Lab 1: Features extraction

:author: Catherine Desrosiers et Felix Remigereau
:date: October 2019
"""
from time import time
import os
import numpy as np
from matplotlib import pyplot as plt
from function_lab1 import *
from sklearn.decomposition import PCA


# ------------------------ 1. Load Data from MNIST and create database structure --------------------------------------
print('--------------------------- Start: Import database --------------------------------')
# UNCOMMENT THIS TO READ THE CSV FILE AND SAVE IT AS NUMPY ARRAY
# Read the given CSV and save the data and label as a numpy array
succeed = load_csv_file_save_as_np('mnist_train.csv', 'train')
if not succeed:
  print('Unable to read CSV file...')
  exit()
succeed = load_csv_file_save_as_np('mnist_test.csv', 'test')
if not succeed:
  print('Unable to read CSV file...')
  exit()


# Load train & test database from numpy file
my_path = os.path.abspath(os.path.dirname(__file__))
train_data = np.load(f'{my_path}\\train_data.npy')
train_label = np.load(f'{my_path}\\train_label.npy')
test_data = np.load(f'{my_path}\\test_data.npy')
test_label = np.load(f'{my_path}\\test_label.npy')
if train_data is None or train_label is None or test_data is None or test_label is None:
    print('Unable to load data from numpy array file')
    exit()
print('--------------------------- End: Import database ----------------------------------')


# --------------------------- 2. Extract and compute overlap for HOG --------------------------------------------------
print('\n-------------------------- Start: Extract HOG Features ----------------------------')
# Creates a dictionary where the key are the cell size in pixel and the value are the block size possibility for that
cell_block_configuration_dic = {14: [1, 2], 7: [1, 2, 4], 4: [2, 3, 4], 2: [2, 4, 8]}
sample_count_overlap = 10000
pca_thresh = 0.95
# Iterates through each size possibilities for cell and block size
for cell_size, block_size_list in cell_block_configuration_dic.items():
    for block_size in block_size_list:
        # Extract databases with HOG as descriptor
        train_database = extract_features(train_data, HOG_METHOD, block_size, cell_size)
        test_database = extract_features(test_data, HOG_METHOD, block_size, cell_size)

        # Save current database to file (as np format)
        np.save(f'{my_path}\\train_HOG_{cell_size}_block{block_size}', train_database)
        np.save(f'{my_path}\\test_HOG_{cell_size}_block{block_size}', test_database)

        # Render descriptor vector size
        print(f'\ncell_size {cell_size}, block_size {block_size} vector size: {train_database.shape[1]}')

        # Compute % of overlapping for training dataset and render it
        # database = np.load(f'{my_path}\\train_HOG_{cell_size}_block{block_size}.npy')  # TODO FIX ME DEBUG STUFF
        overlap = compute_overlap(train_database[0:sample_count_overlap], train_label[0:sample_count_overlap])
        print(f'cell_size {cell_size}, block_size {block_size} overlap: {overlap}\n')

        # Apply PCA using the scikit implementation and apply transformation to test dataset
        pca_HOG = PCA()
        reduced_train_database = compute_principal_components(pca_HOG, train_database, pca_thresh)
        reduced_test_database = pca_HOG.transform(test_database)

print('-------------------------- End: Extract HOG Features --------------------------')

# --------------------------- 2. Extract and compute overlap for LBP --------------------------------------------------
print('\n------------------------ Start: Extract LBP Features --------------------------')
sample_count_overlap = 10000
pca_thresh = 0.95
for block_size in [2, 4, 7, 14]:
    # Extract databases with LBP as descriptor
    train_database = extract_features(train_data, LBP_METHOD, block_size)
    test_database = extract_features(test_data, LBP_METHOD, block_size)

    # Save current database to file (as np format)
    np.save(f'{my_path}\\train_LBP_{block_size}', train_database)
    np.save(f'{my_path}\\test_LBP_{block_size}', test_database)

    # Render descriptor vector size
    print(f'\nblock_size {block_size} vector size: {train_database.shape[1]}')

    # Compute % of overlapping for training dataset and render it
    # database = np.load(f'{my_path}\\feat_{block_size}_LBP.npy')
    overlap = compute_overlap(train_database[0:sample_count_overlap], train_label[0:sample_count_overlap])
    print(f'block_size {block_size} overlap: {overlap}\n')

    # Apply PCA using the scikit implementation and apply transformation to test dataset
    pca_LBP = PCA()
    reduced_train_database = compute_principal_components(pca_LBP, train_database, pca_thresh)
    reduced_test_database = pca_LBP.transform(test_database)

print('\n------------------------ End: Extract LBP Features  --------------------------')
