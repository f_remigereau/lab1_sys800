"""
Here are implemented the function for the lab 1
"""
import csv
import numpy as np
import math
import os
import sys
from matplotlib import pyplot as plt
import time
HOG_METHOD = 'HOG'
LBP_METHOD = 'LBP'
PIXEL_1D_COUNT = 28
EPSILON = 1
BIN_COUNT = 9


def load_csv_file_save_as_np(csv_file_name, output_file_name):
    """
    Load image from the given CSV file and save database and label array as numpy format

    :return: True if the parsing worked, False otherwise
    """

    # Read training data from csv file
    my_path = os.path.abspath(os.path.dirname(__file__))
    lineId = 0
    try:
        with open(f'{my_path}\\{csv_file_name}') as f:
            file = csv.reader(f)
            row_count = len(list(file))
            f.seek(0)
            train_data = np.zeros((row_count, 784), dtype=np.uint8)
            train_label = np.zeros((row_count, 1), dtype=np.uint8)
            for line in file:
                data = np.asarray(line[1:])
                train_data[lineId, :] = data.astype(np.uint8)[:]  # append data and convert it to uin8t
                train_label[lineId] = int(line[0])
                lineId += 1
    except Exception as e:
        print(str(e))
        return False

    np.save(f'{my_path}\\{output_file_name}_data', train_data)
    np.save(f'{my_path}\\{output_file_name}_label', train_label)

    return True


def extract_features(database, method, block_size, cell_size=0):
    """
    Extract wished features (HOG or LBP) and creates a database

    :param database: input image
    :type database: np.ndarray
    :param method: HOG or LBP
    :type method: str
    :param block_size: block size
    :type block_size: int
    :param cell_size: cell size (in pixels), this parameter is not needed for LPB method
    :type cell_size: int

    :return: database extracted features
    :rtype: np.ndarray
    """
    # Valid input ?
    if database is None:
        return None

    features_extracted_database = None
    counter = 0

    # Creates feature vectors for each image in current database
    if method == HOG_METHOD:
        # Instance the database with extracted features
        cell_count = int(PIXEL_1D_COUNT/cell_size)
        nb_covolution_possible = cell_count - block_size + 1
        features_extracted_database = np.ndarray([database.shape[0],
                                                  nb_covolution_possible*nb_covolution_possible*BIN_COUNT],
                                                 dtype=np.float)

        for image in database:
            image = image.reshape((PIXEL_1D_COUNT, PIXEL_1D_COUNT))
            features_extracted_database[counter, :] = extract_HOG(image, cell_size, block_size)
            counter += 1
            update_pct(f'{counter}/{database.shape[0]}')  # Print progress into python console

    elif method == LBP_METHOD:
        features_extracted_database = np.empty((database.shape[0],
                                                int(52*((database[0].shape[0])/block_size**2))),
                                               dtype=np.uint8)
        for image in database:
            image = image.reshape((28, 28))
            features_extracted_database[counter, :] = extract_LBP(image, block_size)
            counter += 1
            update_pct(f'{counter}/{database.shape[0]}')  # Print progress into python console

    return features_extracted_database
def extract_HOG(image, cell_size, block_size):
    """
    Extract features vector following theses steps:
    1- Compute gradient in x and y direction
    2- Compute magnitude and direction
    3- Separate the direction in cell and then compute the histogram of orientation
    4- Regroup cell in block and normalize
    and then return the feature vector

    :param image: input image to compute the gradient on
    :type image: np.ndarray
    :param cell_size: pixel count in x direction that describe the cell size
    :type cell_size: int
    :param block_size: cell count that describe the block size
    :type block_size: int

    :return features_vector: returns the features vector if succeed, None otherwise
    :rtype features_vector: np.ndarray
    """
    # Valid inputs
    # if image is None or image.shape[0] % cell_size != 0:
    #     return None

    # ----------------- 1. Compute gradient in x and y direction   -----------------
    # Creates kernel that will be convoluted over the input image to compute the gradient
    # Specific kernel for image border are also defined
    x_size = image.shape[0]
    y_size = image.shape[1]

    x_kernel = np.asarray([-1, 0, 1])
    y_kernel = x_kernel.reshape((1, 3))
    x_min_kernel = np.asarray([0, 1])
    y_min_kernel = x_min_kernel.reshape((1, 2))
    x_max_kernel = np.asarray([-1, 0])
    y_max_kernel = x_max_kernel.reshape((1, 2))
    gradient_x = np.zeros(image.shape, dtype=np.float)
    gradient_y = np.zeros(image.shape, dtype=np.float)

    # Apply convolution on image  (border are not considered at first)
    for x in range(1, x_size-1):
        for y in range(1, y_size-1):
            gradient_x[x, y] = np.sum(image[x-1:x+2, y]*x_kernel)
            gradient_y[x, y] = np.sum(image[x, y-1:y+2]*y_kernel)
    # Border x min case
    x = 0
    for y in range(1, y_size - 1):
        gradient_x[x, y] = np.sum(image[x:x + 2, y] * x_min_kernel)
        gradient_y[x, y] = np.sum(image[x, y - 1:y + 2] * y_kernel)

    y = 0  # Border x min case and y min
    gradient_x[x, y] = np.sum(image[x:x + 2, y] * x_min_kernel)
    gradient_y[x, y] = np.sum(image[x, y:y + 2] * y_min_kernel)

    y = y_size - 1  # Border x min case and y max
    gradient_x[x, y] = np.sum(image[x:x + 2, y] * x_min_kernel)
    gradient_y[x, y] = np.sum(image[x, y - 1:y + 1] * y_max_kernel)

    # Border x max case
    x = x_size - 1
    for y in range(1, y_size - 1):
        gradient_x[x, y] = np.sum(image[x - 1:x + 1, y] * x_max_kernel)
        gradient_y[x, y] = np.sum(image[x, y - 1:y + 2] * y_kernel)

    y = 0  # Border x max case and y min
    gradient_x[x, y] = np.sum(image[x:x + 2, y] * x_max_kernel)
    gradient_y[x, y] = np.sum(image[x, y:y + 2] * y_min_kernel)

    y = y_size - 1  # Border x max case and y max
    gradient_x[x, y] = np.sum(image[x:x + 2, y] * x_max_kernel)
    gradient_y[x, y] = np.sum(image[x, y - 1:y + 1] * y_max_kernel)

    # Border y min case
    y = 0
    for x in range(1, x_size - 1):
        gradient_x[x, y] = np.sum(image[x - 1:x + 2, y] * x_kernel)
        gradient_y[x, y] = np.sum(image[x, y:y + 2] * y_min_kernel)

    # Border y min case
    y = y_size - 1
    for x in range(1, x_size - 1):
        gradient_x[x, y] = np.sum(image[x - 1:x + 2, y] * x_kernel)
        gradient_y[x, y] = np.sum(image[x, y:y + 2] * y_max_kernel)

    # ----------------- 2. Compute magnitude and direction  -----------------
    magnitude = np.sqrt(gradient_x*gradient_x + gradient_y*gradient_y)
    direction = np.arctan2(gradient_y, gradient_x)*180.0/math.pi  # Convert rad to deg
    direction = np.where(direction < 0, direction+180, direction)  # For angles between -90 and 0[,flip to have + values

    # ----------------- 3- Separate the direction in cell and then compute the histogram of orientation --------------
    # Iterate over the pixel to creates cells
    cell_count = int(image.shape[0]/cell_size)  # In one direction only
    cell_histogram = np.zeros([cell_count, cell_count], dtype=np.ndarray)
    for x_cell in range(0, cell_count):
        x_min_range = x_cell * cell_size
        x_max_range = x_min_range + cell_size
        for y_cell in range(0, cell_count):
            y_min_range = y_cell * cell_size
            y_max_range = y_min_range + cell_size

            # Extract magnitude and direction value
            current_cell_direction = direction[x_min_range:x_max_range, y_min_range:y_max_range]
            current_cell_magnitude = magnitude[x_min_range:x_max_range, y_min_range:y_max_range]

            # Compute the orientation histogram for the current cell
            current_histogram = create_orientation_histo_as_array(current_cell_direction, current_cell_magnitude)
            # if current_histogram is None:
            #     return None

            # Add current histogram to structure
            cell_histogram[x_cell, y_cell] = current_histogram

    # ----------------- 4- Regroup cell in block and normalize ---------------------------------------------------
    nb_convolution_possible = (cell_count - block_size + 1)  # In one direction only
    features_vector = np.zeros(nb_convolution_possible*nb_convolution_possible*BIN_COUNT)
    histogram_vector_size = block_size*block_size*BIN_COUNT

    block_histogram_vector = np.zeros(histogram_vector_size)
    current_id = 0
    for x_block in range(0, nb_convolution_possible):
        x_min_range = x_block
        x_max_range = x_block + block_size
        for y_block in range(0,  nb_convolution_possible):
            y_min_range = y_block
            y_max_range = y_block + block_size
            # Regroup the generate the block and convert to vector
            all_block_histogram: np.ndarray = cell_histogram[x_min_range:x_max_range, y_min_range:y_max_range]
            all_block_histogram = all_block_histogram.flatten()
            for an_histogram_id in range(all_block_histogram.shape[0]):
                block_histogram_vector[an_histogram_id*9:an_histogram_id*9 + 9] = \
                    all_block_histogram[an_histogram_id].flatten()

            # Normalize the current block
            succeed = normalize_block_vector(block_histogram_vector)
            # if not succeed:  # (Those lines are commented for performance purpose)
            #     return None

            # Set feature vector for current block
            features_vector[current_id:(current_id + histogram_vector_size)] = block_histogram_vector[:]
            current_id += histogram_vector_size

    return features_vector


def normalize_block_vector(block_array):
    """
    Returns True if succeed, False otherwise

    :param block_array: block direction vector
    :type block_array: np.ndarray

    :return: normalized block direction vector
    :rtype: np.ndarray

    .. note:: The input array must be a 1D array
    """
    # (Those lines are commented for performance purpose)
    # if block_array is None or len(block_array.shape) > 1:
    #     return False

    # Compute the magnitude of the vector
    k = 0
    for bin in block_array:
        k += bin*bin
    k += EPSILON*EPSILON  # Add epsilon value
    k = math.sqrt(k)

    # Normalize vector
    block_array[:] = block_array/k

    return True


def create_orientation_histo_as_array(cell_direction_data, cell_magnitude_data, bin_count=BIN_COUNT):
    """
    Creates an array that represent an histogram of direction from 0 to 180 deg.
    By default, 9 orientations are selected: 20, 40, 60, 80, 100, 120

    :param cell_direction_data: array of direction (orientation) of the current cell
    :type cell_direction_data: np.ndarray
    :param cell_magnitude_data: array of magnitude of the current cell
    :type cell_magnitude_data: np.ndarray
    :param bin_count: wished bin count for the histogram
    :type bin_count: int

    :return orientation_vector: histogram of direction
    :rtype orientation_vector: np.ndarray
    """
    # Valid input ? (Those lines are commented for performance purpose)
    # if cell_direction_data is None or cell_magnitude_data is None:
    #     return None

    bin_range = int(180/bin_count)
    orientation_vector = np.zeros([bin_count, 1])  # output histogram data

    # Iterate over each pixel of the cell
    for x in range(cell_direction_data.shape[0]):
        for y in range(cell_direction_data.shape[1]):

            # Get the current orientation and orientation for the pixel
            current_orientation = cell_direction_data[x, y]
            current_magnitude = cell_magnitude_data[x, y]

            # Fill the histogram
            for current_bin in range(bin_count):
                min_range = current_bin*bin_range
                max_range = current_bin*bin_range + bin_range
                # Find the associated bin
                if min_range <= current_orientation < max_range:
                    # Compute weights to split the magnitude between the 2 bins
                    distance_from_min = current_orientation-min_range
                    distance_from_max = max_range - current_orientation

                    # Set value for current bin
                    orientation_vector[current_bin] += current_magnitude*(1 - (distance_from_min/bin_range))

                    # Set value for the next bin and come back to first bin if current bin is the last one
                    next_bin = current_bin + 1 if current_bin != bin_count-1 else 0
                    orientation_vector[next_bin] += current_magnitude*(1 - (distance_from_max/bin_range))

    return orientation_vector


def extract_LBP(image, block_size):
    """
    Extract features vector following theses steps:
    1- Compare each pixels to neighbours to compute the local binary pattern
    2- Divide image into blocks and build a histogram for each block
    3- Concatenate histograms to form a single feature vector

    :param image: input image to compute the LBP on
    :type image: np.ndarray
    :param block_size: Size in pixels of each block
    :type block_size: int

    :return features_vector: returns the features vector if succeed, None otherwise
    :rtype features_vector: np.ndarray
    """
    # Valid inputs
    if image is None:
        return
    x_size = image.shape[0]
    y_size = image.shape[1]
    image_out = image.copy()
    for y in range(1, y_size-1):
        for x in range(1, x_size-1):
            lbp_sum = 0
            # Array of the 8 neighbour pixels in the right order
            lbp_array = [
                image[x-1, y-1], image[x, y-1],
                image[x+1, y-1], image[x+1, y],
                image[x+1, y+1], image[x, y+1],
                image[x-1, y+1], image[x-1, y]]
            for i in range(0, 8):
                lbp_array[i] -= image[x, y]  # Compute difference with neighbour pixels
                if lbp_array[i] < 0:
                    lbp_array[i] = 0
                else:
                    lbp_array[i] = 1
                lbp_sum += lbp_array[i]*(2**i)  # Compute decimal LBP value
            image_out[x, y] = lbp_sum
    # Pad the borders with white pixels (value = 255)
    y = 0
    for x in range(0, x_size):
        image_out[x, y] = 255
    y = y_size-1
    for x in range(0, x_size):
        image_out[x, y] = 255
    x = 0
    for y in range(0, y_size):
        image_out[x, y] = 255
    x = x_size-1
    for y in range(0, y_size):
        image_out[x, y] = 255

    block_total_x = x_size/block_size
    block_total_y = y_size/block_size
    # Check if block size is valid
    if not block_total_x.is_integer() or not block_total_y.is_integer():
        print('Block Size is invalid')
        return
    block_total_x = int(block_total_x)
    block_total_y = int(block_total_y)
    final_hist = np.empty(0, dtype=np.uint8)
    # Build histograms for each block and concatenate them to form a single feature vector
    for current_block_x in range(0, block_total_x):
        for current_block_y in range(0, block_total_y):
            temp_hist = np.zeros((52,), dtype=np.uint8)
            for x in range(0, block_size):
                for y in range(0, block_size):
                    lbp_value = image_out[x + block_size * current_block_x, y + block_size * current_block_y]
                    floor_lbp_value = int(lbp_value/5)  # Build histogram with bins of size 5
                    temp_hist[floor_lbp_value] += 1
            # Append histogram of current block to the final vector
            final_hist = np.concatenate((final_hist, temp_hist))
    return final_hist


def compute_overlap(database, labels):
    """
    Computes the % overlap between classes of the database

    :param database: a database
    :type database: np.ndarray
    :param labels: associated label of the selected dataset
    :type labels: np.ndarray

    :return: % of overlapping
    :rtype: float
    """
    # Valid inputs ?
    if database.shape != labels.shape:
        return math.inf

    # Extracts size of the database
    nb_samples, nb_features = database.shape
    output = 0

    # Compute the overlapping
    for i in range(nb_samples):
        update_pct(f'{i}/{nb_samples}')
        distances = np.zeros(nb_samples)

        for j in range(nb_samples):
            distances[j] = math.sqrt(np.sum((database[i, :] - database[j, :])*(database[i, :] - database[j, :])))

        distances[i] = math.inf

        distance_nn = min(distances)
        index_nn = np.where(distances == distance_nn)[0][0]

        if labels[index_nn][0] != labels[i][0]:
            output += 1

    return output/nb_samples*100


def pca(x: np.ndarray, threshold):
    """
    Compute PCA of a distribution.
    :param x:
    :type x: numpy.ndarray
    :param threshold:
    :returns V:
    :returns G:
    """
    # Valid input ?
    if not isinstance(x, np.ndarray):
        return None, None

    # 1) Compute and substract mean
    mean = x.mean()
    x_minus_mean = x - mean

    #  2) Compute covariance
    cov_matrix = np.cov(x_minus_mean)

    # 3) Compute eigenvalues and eigenvectors
    V, D = np.linalg.eig(cov_matrix)

    return V, D


def compute_principal_components(pca_instance, database, threshold):
    """
    Compute PCA

    :param pca_instance:
    :type pca_instance: sklearn.decomposition.PCA
    :param database:
    :type database: np.ndarray
    :param threshold: variance
    :type threshold: float

    :return: new database with dimensionality reduction applied
    :rtype: np.ndarray
    """
    if pca_instance in None or database is None:
        return None

    # Set database to pca instance
    pca_instance.fit(database)

    # Variance
    sigma = np.cumsum(pca_instance.explained_variance_ratio_)
    x = np.arange(sigma.shape[0])

    # Render variance in function of components number
    plt.title('Variabilité exprimée en fonction du nombre de composantes')
    plt.plot(x, sigma)
    plt.ylabel('% de variabilité exprimée')
    plt.xlabel('Nombre de composantes utilisées')
    plt.show()

    # Find number of components
    r = np.where(sigma > threshold)
    nb_components = r[0][0] + 1
    pca_instance.n_components = nb_components
    new_database = pca_instance.fit_transform(database)

    return new_database


def update_pct(w_str):
    """
    Function to show progress status in the console
    """
    w_str = str(w_str)
    sys.stdout.write("\b" * len(w_str))
    sys.stdout.write(" " * len(w_str))
    sys.stdout.write("\b" * len(w_str))
    sys.stdout.write(w_str)
    sys.stdout.flush()
